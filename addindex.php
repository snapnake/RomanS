<?php

class Gen{
    public $arr = array([array()]);

    // Функція для виводу всіх елементів масиву
    public function print_number($array_gen,$str){
        for($i = 0; $i < count($array_gen); $i++){
            for($j = 0; $j < count($array_gen[$i]); $j++){
                 echo '<div style="display: inline-block;width:100px;margin:10px 0px;">'. $array_gen[$i][$j] . '</div>';
            }
            echo "<br />";
        }
    }

    // Генератор чисел Фібоначчі
    public function fib($n,$m){
        if($n == 1 AND $m == 1){
            $arr[0][0] = 0;
            return $arr;
        }
        $add_v1 = $arr[0][0] = 0;
        $add_v2 = $arr[0][1] = 1;
        for($i = 0; $i < $n; $i++){
            if($i != 0){
                $arr[$i][0]= $add_v1 + $add_v2;
            }
            for($j = 1; $j < $m; $j++){
                if($j < 2 AND $i != 0){
                    $arr[$i][$j] = $arr[$i][$j-1] + $add_v1;
                }
                if($j >= 2){
                    $arr[$i][$j] = $arr[$i][$j-1] + $arr[$i][$j-2];
                }
                $add_v1 = $arr[$i][$j];
                $add_v2 = $arr[$i][$j-1];
            }
        }
        return $arr;
    }

    // Генератор чисел Мерсена
    public function mer($n,$m){
        $k = 0;
        for($i = 0; $i < $n; $i++){
            for($j = 0; $j < $m; $j++){
                $arr[$i][$j] = pow(2, $k+1) - 1;
                $k++;
            }
        }

        return $arr;
    }

    //Генератор рандомних чисел
    public function ran($n,$m){
        for($i = 0; $i < $n; $i++){
            for($j = 0; $j < $m; $j++){
                $arr[$i][$j] = rand(0,999);
            }
        }
        return $arr;
    }

    //Генератор простих чисел
    public function smp($n,$m){
        $numbers = 3;
        for($i = 0; $i < $n; $i++){
            for($j = 0; $j < $m; $j++){
                if($i == 0 AND $j == 0){
                    $arr[0][0] = 2;
                    $j++ ;
                }
                for($dil = 2; $dil <= $numbers/2 + 1; $dil++){
                    if($numbers % $dil == 0){
                        $numbers++;
                        $dil = 2;
                        continue;
                    }
                    if($dil > $numbers/2 ){
                        $arr[$i][$j] = $numbers;
                        $numbers++;
                        break;
                    }
                }
            }
        }
        return $arr;
    }

    //Функція для запису чисел в файл
    public function gen_file($arr_file,$str){
        $name_file = "addfile.txt";
        $save = fopen($name_file,'a');

        for($i = 0; $i < count($arr_file); $i++){
            for($j = 0; $j < count($arr_file[$i]); $j++){
                fwrite($save,$arr_file[$i][$j]." | ");
            }
            fwrite($save,PHP_EOL);
        }
        fwrite($save, "Результат генерації ".$str);
        fwrite($save,PHP_EOL);
        fwrite($save,PHP_EOL);
        fclose($save);
    }
}
?>
<head>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>

<form action="addindex.php" method="get">
    <label>Введіть кількість рядків:
        <input type="text" name = "numb1"  class = "inp_num"><br>
    </label>
    <label>Введіть кількість стовбців:
        <input type="text" name = "numb2"  class = "inp_num"><br>
    </label>
    <input type="checkbox" name="gen_file" value="a2">Записати числа в файл<Br>
    <input type="submit" name = "gen_ran" value="Рандомні числа" class = "sub">
    <input type="submit" name = "gen_smp" value="Прості числа" class = "sub">
    <input type="submit" name = "gen_fib" value="Числа Фібоначчі" class = "sub">
    <input type="submit" name = "gen_mer" value="Числа Мерсена" class = "sub"><br>
</form>

<div class = "result">

    <?php
    $result = array();
    if(isset($_GET['numb1']) AND isset($_GET['numb2']) AND $_GET['numb1'] > 0 AND $_GET['numb1'] < 100
        AND $_GET['numb2'] > 0 AND $_GET['numb2'] < 100){
        $numb1 = $_GET['numb1'];
        $numb2 = $_GET['numb2'];
        if(isset($_GET['gen_ran'])){
            $str = "рандомних чисел";
            $gen = new Gen;
            $result = $gen->ran($numb1,$numb2);
            $gen->print_number($result,$str);
            if (isset($_GET['gen_file'])){
                $gen->gen_file($result,$str);
            }
        }
        if(isset($_GET['gen_smp'])){
            $str = "простих чисел";
            $gen = new Gen;
            $result = $gen->smp($numb1,$numb2);
            $gen->print_number($result, $str);
            if (isset($_GET['gen_file'])){
                $gen->gen_file($result,$str);
            }
        }
        if(isset($_GET['gen_fib'])){
            $str = "чисел Фібоначчі";
            $gen = new Gen;
            if($numb1 > 0 AND $numb2 > 1){
                $result = $gen->fib($numb1,$numb2);
                $gen->print_number($result, $str);
            }else{
                echo "Числа введено не правильно!";
            }

            if (isset($_GET['gen_file'])){
                $gen->gen_file($result,$str);
            }
        }
        if(isset($_GET['gen_mer'])){
            $str = "чисел Мерсена";
            $gen = new Gen;
            $result = $gen->mer($numb1,$numb2);
            $gen->print_number($result,$str);
            if (isset($_GET['gen_file'])){
                $gen->gen_file($result,$str);
            }
        }
    }else{
        echo "Введіть кількість чисел для генерації min = 1, max = 99";
    }
    ?>
</div>

<div style="display: block; text-align: center;">
    <button style="display: inline-block; margin: 20px 10px;"><a href="index.php">Головна</a></button>
    <button style="display: inline-block;  margin: 20px 10px;"><a href="addindex.php">Двовимірний масив</a></button>
</div>