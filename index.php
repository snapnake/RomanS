<?php


class Gen{
      public $arr = array();

      // Функція для виводу всіх елементів масиву
      public function print_number($array_gen,$str){
          $i = 0;
          foreach ($array_gen as &$value) {
               echo '<span>' . $value ."  -  " .'</span>';
               $i++;
          }
          echo '<span>' ."Результат генерації " . $str . "(Кількість згенерованих чисел - " .$i ." )".'</span>';
      }

      // Генератор чисел Фібоначчі
      public function fib($n){
          if($n == 1){
              $arr[0] = 0;
              return $arr;
          }
          $arr[0] = 0;
          $arr[1] = 1;
          for($i = 2; $i < $n; $i++){
              $arr[$i] = $arr[$i-1] + $arr[$i-2];
          }
          return $arr;
      }

      // Генератор чисел Мерсена
      public function mer($n){
          for($i = 0; $i < $n; $i++){
              $arr[$i] = pow(2, $i+1) - 1;
          }
          return $arr;
      }

      //Генератор рандомних чисел
      public function ran($n){
          for($i = 0; $i < $n; $i++){
              $arr[$i] = rand(0,999);
          }
          return $arr;
      }

      //Генератор простих чисел
      public function smp($n){
          $numbers = 3;
          $arr[0] = 2;
          for($i = 1; $i < $n; $i++){
              for($dil = 2; $dil < $numbers/2 + 1; $dil++){
                  if($numbers % $dil == 0){
                      $numbers++;
                      $dil = 2;
                      continue;
                  }
                  if($dil > $numbers / 2 ){
                      $arr[$i] = $numbers;
                      $numbers++;
                      break;
                  }
              }
          }
          return $arr;
      }

      //Функція для запису чисел в файл
      public function gen_file($arr_file,$str){
          $name_file = "file.txt";
          $save = fopen($name_file,'a');
          $i = 1;
          foreach ($arr_file as &$value) {
               fwrite($save,$value);
               fwrite($save," | ");
               if ($i%10 == 0){
                   fwrite($save,PHP_EOL);
               }
               $i++;
          }
          fwrite($save, "Результат генерації ".$str);
          fwrite($save,PHP_EOL);
          fwrite($save,PHP_EOL);
          fclose($save);
      }
  }
?>
<head>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>

<form action="index.php" method="get">
    <label>Введіть кількість чисел, які необхідно згенерувати:
    <input type="text" name = "numb"  class = "inp_num"><br>
    </label>
    <input type="checkbox" name="gen_file" value="a2">Записати числа в файл<Br>
    <input type="submit" name = "gen_ran" value="Рандомні числа" class = "sub">
    <input type="submit" name = "gen_smp" value="Прості числа" class = "sub">
    <input type="submit" name = "gen_fib" value="Числа Фібоначчі" class = "sub">
    <input type="submit" name = "gen_mer" value="Числа Мерсена" class = "sub"><br>
</form>

<div class = "result">

    <?php
    $result = array();
    if(isset($_GET['numb']) AND $_GET['numb'] > 0 AND $_GET['numb'] < 100){
        $numb = $_GET['numb'];
        if(isset($_GET['gen_ran'])){
            $str = "рандомних чисел";
            $gen = new Gen;
            $result = $gen->ran($numb);
            $gen->print_number($result, $str);
            if (isset($_GET['gen_file'])){
                $gen->gen_file($result,$str);
            }
        }
        if(isset($_GET['gen_smp'])){
            $str = "простих чисел";
            $gen = new Gen;
            $result = $gen->smp($numb);
            $gen->print_number($result, $str);
            if (isset($_GET['gen_file'])){
                $gen->gen_file($result,$str);
            }
        }
        if(isset($_GET['gen_fib'])){
            $str = "чисел Фібоначчі";
            $gen = new Gen;
            $result = $gen->fib($numb);
            $gen->print_number($result, $str);
            if (isset($_GET['gen_file'])){
                $gen->gen_file($result,$str);
            }
        }
        if(isset($_GET['gen_mer'])){
            $str = "чисел Мерсена";
            $gen = new Gen;
            $result = $gen->mer($numb);
            $gen->print_number($result,$str);
            if (isset($_GET['gen_file'])){
                $gen->gen_file($result,$str);
            }
        }
    }else{
        echo '<span class = "red_error">' . "Введіть кількість чисел для генерації min = 1, max = 99" . '</span>';
    }
    ?>
</div>

<div style="display: block; text-align: center;">
   <button style="display: inline-block; margin: 20px 10px;"><a href="index.php">Головна</a></button>
   <button style="display: inline-block;  margin: 20px 10px;"><a href="addindex.php">Двовимірний масив</a></button>
</div>

<script type="text/javascript" src ="jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="script.js"></script>